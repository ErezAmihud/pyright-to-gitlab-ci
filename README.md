# pyright-to-gitlab-ci

A program that converts pyright json output to gitlab ci codeclimate to be able to use it in the gitlab ci reports.
This was not merged to the main pyright repo because of the [maintainer preference](https://github.com/microsoft/pyright/issues/4628). 

[!["Buy Me A Coffee"](https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png)](https://buymeacoffee.com/erezamihud)

### Installation
`npm i -g pyright-to-gitlab-ci`

### Usage
```
pyright <python source> --outputjson > report_raw.json
pyright-to-gitlab-ci --src report_raw.json --output report.json --base_path .
```
This converts the pyright report json to a codeclimate (gitlab) compatible json. It also changes the absolute paths to be a relative path to fit gitlab requirements.

Usage in a gitlab ci job:
```
job_name:
  before_script:
    - npm i -g pyright
    - npm i -g pyright-to-gitlab-ci
  script:
   - pyright <python source> --outputjson > report_raw.json
   - pyright-to-gitlab-ci --src report_raw.json --output report.json --base_path .
  artifacts:
    paths:
      - report.json
    reports:
      codequality: report.json
```

### Supported pyright versions
This package support all known formats, the format before v1.1.340 and after.