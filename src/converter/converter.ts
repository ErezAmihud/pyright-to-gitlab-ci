import path from "node:path"
import type { Gitlab, Pyright } from "../types";
import * as crypto from "crypto";

export function convertPosition(position: Pyright.Position) : Gitlab.CodeClimatePosition{
    return {
        line: position.line+1, // pyright count start at 0 while codeclimate start at 1
        column: position.character
    }
}

export function convertRange(range: Pyright.Range): Gitlab.CodeClimateRange{
    return {
        begin: convertPosition(range.start),
        end: convertPosition(range.end)
    }
}


export function convertDiagnosticCategoryToGitlabSeverity(category: Pyright.SeverityLevel): Gitlab.GitlabSeverityLevel {
    switch (category) {
        case "error": {
            return 'blocker';
        }
        case "warning":{
            return "major";
        }
        case "information":{
            return 'info';
        }
        case "unreachablecode":{
            return 'minor';
        }
        case "unusedcode":{
            return 'info';
        }
        case "deprecated":{
            return "info"
        }
    }
}


export function extract_path(diag: Pyright.PyrightJsonDiagnostic): string {
    if (diag.uri !== undefined) {
        return diag.uri._filePath;
    } else if (diag.file !== undefined) {
        return diag.file;
    } else { throw new Error("file and uri fields are undefined, probably new file format, contact developer"); }
}

export function convertDiagnosticToGitlab(diag: Pyright.PyrightJsonDiagnostic, base_path: string): Gitlab.GitlabReport {
    let extracted_path = extract_path(diag);
    let d = {
        description: diag.message,
        fingerprint: "",
        severity: convertDiagnosticCategoryToGitlabSeverity(diag.severity),
        location: {
            path: path.relative(path.resolve(base_path), extracted_path),
            positions: diag.range === undefined ? { begin: { column: 1, line: 1 }, end: { column: 1, line: 1 } } : convertRange(diag.range)
        },
    };

    // Use a hash of the data as the fingerprint to ensure it's unique.
    // https://docs.gitlab.com/ee/ci/testing/code_quality.html#implement-a-custom-tool
    d["fingerprint"] = crypto.createHash('md5').update(JSON.stringify(d)).digest('hex');

    return d;
}

export function convertDiagnostics(diag: Pyright.PyrightJsonDiagnostic[], base_path: string): Gitlab.GitlabReport[] {
    const response: Gitlab.GitlabReport[] = []
    for(const value of diag){
        response.push(convertDiagnosticToGitlab(value, base_path))
    }
    return response
}
