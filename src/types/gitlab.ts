export type GitlabSeverityLevel = 'info' | 'minor' | 'major' | 'critical' | 'blocker';

export interface CodeClimatePosition{
    line: number
    column: number
}

export interface CodeClimateRange{
    begin: CodeClimatePosition,
    end: CodeClimatePosition,
}
export interface GitlabLocation{
    path: string
    positions: CodeClimateRange
}

export interface GitlabReport{
    description: string
    fingerprint: string
    severity: GitlabSeverityLevel
    location: GitlabLocation
}