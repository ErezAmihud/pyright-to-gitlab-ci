export type SeverityLevel = "error" | "warning" | "information" | "unusedcode" | "unreachablecode" | "deprecated"

export interface Position {
    // Both line and column are zero-based
    line: number;
    character: number;
}

export interface Range {
    start: Position;
    end: Position;
}

interface Uri{
    _key: string;
    _filePath: string;
}

export interface PyrightJsonDiagnostic {
    file?: string;
    uri?: Uri;
    severity: SeverityLevel;
    message: string;
    range?: Range | undefined;
    rule?: string | undefined;
}

export interface GeneralReport {
    generalDiagnostics: PyrightJsonDiagnostic[]
}
